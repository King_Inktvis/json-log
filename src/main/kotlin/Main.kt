import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Row
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowPosition
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import data.IngestQueue
import data.JsonHolder
import data.ViewData
import kotlinx.coroutines.channels.Channel

enum class View {
    DATA, INPUT, SETTINGS
}

@Composable
@Preview
fun App(list: ViewData, collector: DataCollector, settings: Settings) {
    val location = remember { mutableStateOf(collector.settings.location) }
    val command = remember { mutableStateOf(collector.settings.command) }
    val state = remember { mutableStateOf(View.INPUT) }
    MaterialTheme {
        Scaffold(
            topBar = {
                TopAppBar() { header(state) }
            }
        ) {
            if (state.value == View.INPUT) {
                inputView(collector, location, command)
            } else if (state.value == View.DATA) {
                dataView(list)
            } else {
                settingsView(settings)
            }
        }
    }
}

@Composable
private fun dataView(list: ViewData) {
    Row {
        filterFields(list)
        dataDisplay(list)
    }
}

fun main() {
    val settings = Settings()
    val chan = Channel<Boolean>(1000)
    val jsonList = JsonHolder(chan)
    jsonList.run()
    val dataView = ViewData(jsonList, chan)
    dataView.run()
    val ingest = IngestQueue(jsonList.dataReceiver)
    ingest.run(settings.deserializeWorkers)
    val collector = DataCollector(settings, ingest.read)
    collector.loadDataFromStdin()
    application {
        val state = rememberWindowState(
            size = DpSize(settings.windowWidth.dp, settings.windowHeight.dp),
            position = WindowPosition(settings.windowPositionX.dp, settings.windowPositionY.dp)
        )

        Window(title = "Json log", state = state, onCloseRequest = {
            collector.collectors.value.forEach { it.stop() }
            settings.windowWidth = state.size.width.value
            settings.windowHeight = state.size.height.value
            settings.windowPositionX = state.position.x.value
            settings.windowPositionY = state.position.y.value
            exitApplication()
        }) {
            App(dataView, collector, settings)
        }
    }
}
