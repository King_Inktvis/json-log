package data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject

class IngestQueue(val storeReceiver: Channel<JsonObject>) {
    val read: Channel<String> = Channel(10)

    fun run(workers: Int) {
        GlobalScope.launch {
            withContext(Dispatchers.Default) {
                for (i in 1..workers) {
                    launch {
                        for (value in read) {
                            compute(value)
                        }
                    }
                }
            }
        }
    }

    suspend fun compute(value: String) {
        try {
            val data = Json.decodeFromString<JsonObject>(value)
            storeReceiver.send(data)
        } catch (e: Exception) {
            println(e)
        }
    }
}
