import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class CollectorDetails(
    val process: Process,
    val location: String,
    val command: String,
    val running: MutableState<Boolean> = mutableStateOf(true)
) {
    fun stop() {
        this.process.destroy()
        this.running.value = false
    }
}

class DataCollector(val settings: Settings, val notify: SendChannel<String>) {

    val collectors = mutableStateOf(listOf<CollectorDetails>())


    fun loadDataFromStdin() {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                launch {
                    try {
                        while (true) {
                            val value = readLine()
                            if (value == null) {
                                println("end stdin")
                                break
                            }
                            notify.send(value)
                        }
                    } catch (e: Exception) {
                        println(e)
                    }
                }
            }
        }
    }

    fun runCommand(command: String, location: String) {
        settings.location = location
        settings.command = command
        val loc = File(location)
        val builder: Process
        try {
            builder = ProcessBuilder(command.split(" "))
                .directory(loc).start()
        } catch (e: Exception) {
            println(e)
            return
        }
        val obj = CollectorDetails(builder, location, command)
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val reader = builder.inputStream.bufferedReader()
                    while (true) {
                        val line = reader.readLine()
                        if (line == null) {
                            val t = collectors.value.find { it == obj }
                            t?.stop()
                            return@withContext
                        }
                        notify.send(line)
                    }

                } catch (e: Exception) {
                    println(e)
                }
            }
        }
        collectors.value = collectors.value + listOf(obj)
    }
}
