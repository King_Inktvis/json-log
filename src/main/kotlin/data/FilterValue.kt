package data

class FilterValue(
    var visible: Boolean = true,
    var filter: String = ""
) {
    fun clone(): FilterValue {
        return FilterValue(this.visible, this.filter)
    }
}
