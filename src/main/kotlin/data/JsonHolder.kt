package data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.JsonObject

class JsonHolder(val notify: SendChannel<Boolean>) {
    var data = mutableListOf<JsonObject>()
    val dataReceiver = Channel<JsonObject>(100)

    private suspend fun add(newValue: JsonObject) {
        this.data.add(newValue)
        notify.send(true)
    }

    fun run() {
        GlobalScope.launch {
            withContext(Dispatchers.Default) {
                for (value in dataReceiver) {
                    add(value)
                }
            }
        }
    }
}
