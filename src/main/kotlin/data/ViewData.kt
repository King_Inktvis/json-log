package data

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.mutableStateOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.JsonObject

class ViewData(val store: JsonHolder, val listen: Channel<Boolean>) {

    val filteredData = mutableStateOf(listOf<JsonObject>())
    val filters = mutableStateOf(mapOf<String, FilterValue>())
    val scrollState = LazyListState()
    var toBottom = true

    fun run() {
        GlobalScope.launch {
            withContext(Dispatchers.Default) {
                try {
                    for (i in listen) {
                        while (!listen.isEmpty) {
                            listen.tryReceive()
                        }
                        toBottom =
                            scrollState.layoutInfo.visibleItemsInfo.lastOrNull()?.index == scrollState.layoutInfo.totalItemsCount - 1 ||
                                    scrollState.layoutInfo.visibleItemsInfo.lastOrNull() == null ||
                                    scrollState.isScrollInProgress
                        genKeys()
                        genFilteredList()
                    }
                } catch (e: Exception) {
                    println(e)
                }
            }
        }
    }

    private fun genKeys() {
        val newKeys = mutableMapOf<String, FilterValue>()
        for (i in 0..store.data.lastIndex) {
            val v = store.data[i]
            for (k in v.keys) {
                if (!filters.value.contains(k)) {
                    newKeys[k] = FilterValue()
                }
            }
        }
        filters.value = newKeys + filters.value
    }

    private fun genFilteredList() {
        val newList = mutableListOf<JsonObject>()
        val toBlock = filters.value.entries.filter { it.value.filter != "" }
        val visibleKeys = filters.value.entries.filter { it.value.visible }.map { it.key }
        for (i in 0..store.data.lastIndex) {
            val data = store.data[i]
            if (objectIsAllowed(data, toBlock, visibleKeys)) {
                newList.add(data)
            }
        }
        filteredData.value = newList
    }

    private fun objectIsAllowed(
        data: JsonObject,
        toBlock: List<Map.Entry<String, FilterValue>>,
        visibleKeys: List<String>
    ): Boolean {
        for (f in toBlock) {
            val k = data[f.key] ?: return false
            if (!k.toString().contains(f.value.filter)) {
                return false
            }
        }
        for (key in visibleKeys) {
            if (data[key] != null) {
                return true
            }
        }
        return false
    }

    suspend fun setFilter(key: String, value: String) {
        val old = (filters.value[key] ?: FilterValue()).clone()
        old.filter = value
        val new = mapOf(key to old)
        this.filters.value = this.filters.value + new
        listen.send(true)
    }

    suspend fun setKeyVisibility(key: String, value: Boolean) {
        val old = (filters.value[key] ?: FilterValue()).clone()
        old.visible = value
        val new = mapOf(key to old)
        this.filters.value = this.filters.value + new
        listen.send(true)
    }

    suspend fun setAllVisibilities(value: Boolean) {
        val newMap = mutableMapOf<String, FilterValue>()
        this.filters.value.forEach {
            val new = it.value.clone()
            new.visible = value
            newMap[it.key] = new
        }
        this.filters.value = newMap
        listen.send(true)
    }
}
