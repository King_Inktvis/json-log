import java.util.prefs.Preferences

class Settings {
    val prefs = Preferences.userNodeForPackage(Class.forName("Settings"))

    var location: String = prefs.get("LOCATION", "")
        set(value) {
            prefs.put("LOCATION", value)
            field = value
        }

    var command: String = prefs.get("COMMAND", "")
        set(value) {
            prefs.put("COMMAND", value)
            field = value
        }

    var windowWidth: Float = prefs.get("WINDOW_WIDTH", "").toFloatOrNull() ?: 900f
        set(value) {
            prefs.put("WINDOW_WIDTH", value.toString())
            field = value
        }

    var windowHeight: Float = prefs.get("WINDOW_HEIGHT", "").toFloatOrNull() ?: 500f
        set(value) {
            prefs.put("WINDOW_HEIGHT", value.toString())
            field = value
        }

    var windowPositionX: Float = prefs.get("WINDOW_POSITION_X", "").toFloatOrNull() ?: 0f
        set(value) {
            prefs.put("WINDOW_POSITION_X", value.toString())
            field = value
        }

    var windowPositionY: Float = prefs.get("WINDOW_POSITION_Y", "").toFloatOrNull() ?: 0f
        set(value) {
            prefs.put("WINDOW_POSITION_Y", value.toString())
            field = value
        }

    var deserializeWorkers: Int = prefs.get("DESERIALIZE_WORKERS", "").toIntOrNull() ?: 2
        set(value) {
            if (value > 0) {
                prefs.put("DESERIALIZE_WORKERS", value.toString())
                field = value
            }
        }
}
