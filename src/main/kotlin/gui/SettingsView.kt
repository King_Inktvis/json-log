import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.text.input.KeyboardType

@Composable
fun settingsView(settings: Settings) {
    Column {
        Row {
            Text("Deserialize workers")
        }
        Row {
            val deserializeWorkers = remember { mutableStateOf(settings.deserializeWorkers.toString()) }
            TextField(
                value = deserializeWorkers.value,
                onValueChange = {
                    val tmp = it.toIntOrNull()
                    if (tmp != null) {
                        settings.deserializeWorkers = tmp
                    }
                    if (tmp != null || it == "") {
                        deserializeWorkers.value = it
                    }
                },
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
            )
        }
    }
}
