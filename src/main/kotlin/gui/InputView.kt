import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState

@Composable
fun inputView(
    collector: DataCollector,
    location: MutableState<String>,
    command: MutableState<String>
) {
    Column {
        runningCollectors(collector)
        newCommand(location, command, collector)
    }
}

@Composable
private fun newCommand(
    location: MutableState<String>,
    command: MutableState<String>,
    collector: DataCollector
) {
    Row {
        Row {
            Column { Text("location") }
            Column { TextField(location.value, { location.value = it }) }
            Column { Text("command") }
            Column { TextField(command.value, { command.value = it }) }
            Column {
                Button(onClick = {

                    collector.runCommand(
                        command.value,
                        location.value
                    )

                }) { Text("Run") }
            }
        }
    }
}

@Composable
private fun runningCollectors(collector: DataCollector) {
    for (i in collector.collectors.value) {
        Row {
            if (i.running.value) {
                Text("Running: ${i.command} @ ${i.location}")
                Button(onClick = { i.stop() }) {
                    Text("Stop")
                }
            } else {
                Text("Stopped: ${i.command} @ ${i.location}")
            }
        }
    }
}
