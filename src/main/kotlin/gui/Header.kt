import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState

@Composable
fun header(
    state: MutableState<View>
) {
    val views = View.values()
    TabRow(selectedTabIndex = views.indexOf(state.value)) { // 3.

        views.forEach { title ->
            Tab(selected = state.value == title, // 4.
                onClick = { state.value = title },
                text = { Text(text = title.toString()) }) // 5.
        }
    }
}