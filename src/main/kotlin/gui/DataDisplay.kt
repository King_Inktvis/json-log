import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import data.FilterValue
import data.ViewData
import kotlinx.coroutines.launch
import kotlinx.serialization.json.JsonObject

@Composable
fun dataDisplay(viewData: ViewData) {
    val visible = viewData.filters.value
    val list = viewData.filteredData.value
    Box(
        modifier = Modifier.fillMaxHeight()
            .background(color = Color(180, 180, 180))
            .padding(10.dp)
    ) {
        val scope = rememberCoroutineScope()

        if ((viewData.toBottom && list.lastIndex != -1)) {
            scope.launch {
                if (viewData.scrollState.firstVisibleItemIndex < list.size - 20) {
                    viewData.scrollState.scrollToItem(list.lastIndex)
                } else {
                    viewData.scrollState.animateScrollToItem(list.lastIndex)
                }
            }
        }
        LazyColumn(Modifier.fillMaxHeight().padding(end = 12.dp), viewData.scrollState) {
            items(list.size) { x ->
                displayJsonEntry(list[x], visible)
            }
        }
        VerticalScrollbar(
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            adapter = rememberScrollbarAdapter(
                scrollState = viewData.scrollState
            )
        )
    }
}

@Composable
private fun displayJsonEntry(item: JsonObject, list: Map<String, FilterValue>) {
    Row(Modifier.border(border = BorderStroke(width = 1.dp, Color.LightGray)).fillMaxWidth()) {
        SelectionContainer {
            Column {
                for (entry in item.entries) {
                    if (list[entry.key]?.visible == true) {
                        Row {
                            Text("$entry")
                        }
                    }
                }
            }
        }
    }
}
