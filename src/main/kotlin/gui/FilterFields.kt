import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import data.ViewData
import kotlinx.coroutines.launch

@Composable
fun filterFields(list: ViewData) {
    Box(
        modifier = Modifier.fillMaxHeight()
            .width(300.dp)
            .background(color = Color(180, 180, 180))
            .padding(10.dp)
    ) {

        val state = rememberScrollState()
        val scope = rememberCoroutineScope()
        Column(Modifier.fillMaxHeight().padding(end = 12.dp).verticalScroll(state = state)) {
            Row {
                Button(onClick = { scope.launch { list.setAllVisibilities(true) } }) { Text("All") }
                Button(onClick = { scope.launch { list.setAllVisibilities(false) } }) { Text("None") }
            }
            val keys = list.filters.value.keys.toList()
            keys.forEach { key ->
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Checkbox(
                        list.filters.value[key]!!.visible,
                        { scope.launch { list.setKeyVisibility(key, !list.filters.value[key]!!.visible) } })
                    Text(key)
                }
                Row {
                    TextField(list.filters.value[key]!!.filter, { scope.launch { list.setFilter(key, it) } })
                }
            }
        }
        VerticalScrollbar(
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            adapter = rememberScrollbarAdapter(
                scrollState = state
            )
        )
    }
}
